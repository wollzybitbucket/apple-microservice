from flask import Flask, request, jsonify
from callback import *


app = Flask(__name__)


def make_error(code, message):
    err = {
        'error': {
            'code': code,
            'message': message
        },
        'status': 'Error',
        'data': ''
    }
    return jsonify(err), code


def get_pass_db(partner_id, skip, limit):
    return 'search results matching criteria'


def create_pass_db(data):
    return 'pass created'


@app.route('/pass', methods=['GET'])
def get_pass():
    partner_id = None
    skip = None
    limit = None
    if 'partner_id' in request.args:
        partner_id = request.args.get('partner_id')
    if 'skip' in request.args:
        skip = request.args.get('skip')
    if 'limit' in request.args:
        limit = request.args.get('limit')
    return get_pass_db(partner_id, skip, limit)    


@app.route('/pass', methods=['POST'])
def post_pass():
    data = request.json
    if data and all([field in data for field in ['serial_number_id', 'parent_serial_number_id']]):
        return create_pass_db(data)
    else:
        return make_error(400, 'serial_number_id and parent_serial_number_id are required')


if __name__ == '__main__':
    app.add_url_rule('/ios/<version>/devices/<deviceLibraryIdentifier>/registrations/<passTypeIdentifier>/<serialNumber>','registrate', registrate_device, methods=['POST'])
    app.add_url_rule('/ios/<version>/devices/<deviceLibraryIdentifier>/registrations/<passTypeIdentifier>/<serialNumber>','registrate', unregisterate_device, methods=['DELETE'])
    app.add_url_rule('/ios/<version>/devices/<deviceLibraryIdentifier>/registrations/<passTypeIdentifier>', get_serial_number, methods=['GET'])
    app.add_url_rule('/ios/<version>/passes/<passTypeIdentifier>/<serialNumber>', get_latest_version_pass, methods=['GET'])
    app.add_url_rule('/ios/<version>/log', log_errors, methods=['POST'])
    app.run(host='0.0.0.0', port=5000, debug=True)
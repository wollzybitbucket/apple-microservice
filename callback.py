from flask import jsonify, request, send_file
from passbook.models import Pass, Barcode, StoreCard


ACTIVE = 1
WAIT_FINISH_REG = 2


def get_pass_db(serialNumber=None, passTemplate=None, pass_ids=None, status=None):
    return {
        'id': 1, 
        'authentication_token': 13123123,
        'serial_numebr': 12334113,
        'pass_updated_since': 123123,
    }


def find_device_db(pushToken):
    return None


def registrate_device_db(data):
    return {
        'id':1,
    }


def get_device_library_identifier_db(deviceLibraryIdentifier, status=None):
    pass


def create_device_library_identifier_db(deviceLibraryIdentifier, device_id):
    return {
        'id': 1,
        'libraryPassess': []
    }


def get_library_pass_db(deviceLI_id, pass_id, status=None):
    pass


def create_library_pass_db(deviceLI_id, pass_id):
    return {
        'id': 1,
    }


def get_pass_type_identifiers_db(passTypeIdentifier, status=None):
    pass


def get_pass_template_db(pass_type_identifier, status=None):
    pass


def create_file_pass(pass_, passTypeIdentifier, serialNumber):
    cardInfo = StoreCard()

    organizationName = 'Wollzy' # Имя организации заполнить
    teamIdentifier = 'AGK5BZEN3E' # Нужен ли?

    passfile = Pass(cardInfo, \
        passTypeIdentifier=passTypeIdentifier, \
        organizationName=organizationName, \
        teamIdentifier=teamIdentifier)

    passfile.serialNumber = serialNumber 
    passfile.barcode = Barcode(message = '') # Сообщение неообходимо заполнить

    passfile.addFile('icon.png', open('images/icon.png', 'r')) # Нужно иконка и лого
    passfile.addFile('logo.png', open('images/logo.png', 'r'))
    
    password = '123456'
    name_of_file = '{}.pkpass'.format(str(passTypeIdentifier) + str(serialNumber))
    passfile.create('certificate.pem', 'key.pem', 'wwdr.pem', password , name_of_file) 

    return name_of_file


def registrate_device(version, deviceLibraryIdentifier, passTypeIdentifier, serialNumber):
    data = request.json
    # Check header
    pass_ = get_pass_db(serialNumber)
    if not pass_:
        return 'Error', 404
    if pass_['authentication_token'] != request.headers.get('Authorization'):
        return '', 401
    if 'pushToken' in data:
        device = find_device_db(data['pushToken']) 
        if not device:
            device = registrate_device_db(data['pushToken'])
            
        deviceLI = get_device_library_identifier_db(deviceLibraryIdentifier)
        if not deviceLI:
            deviceLI = create_device_library_identifier_db(deviceLibraryIdentifier, device['id'])
        
        libraryPass = get_library_pass_db(deviceLI['id'], pass_['id'])
        if not libraryPass:
            libraryPass = create_library_pass_db(deviceLI['id'], pass_['id'])
        return 'Ok', 201


def get_serial_number(version, deviceLibraryIdentifier, passTypeIdentifier):
    # find serial number by device library identifier
    deviceLi = get_device_library_identifier_db(deviceLibraryIdentifier)
    if not deviceLi:
        return 'Error',404
    pass_ids = get_library_pass(deviceLi['id'])
    pass_type_identifier = get_pass_type_identifiers_db(passTypeIdentifier)
    if not pass_type_identifier:
        return 'Error', 404
    passTemplate = get_pass_template_db(pass_type_identifier)
    passes = get_pass_db(template_id=passTemplate['id'], pass_ids=pass_ids)
    return jsonify(passes)


def get_latest_version_pass(version, passTypeIdentifier, serialNumber):
    
    pass_ = get_pass_db(serialNumber=serialNumber)
    if not pass_:
        return 'Pass not fonud', 404
    
    if pass_['authentication_token'] != request.headers.get('Authorization'):
        return '', 401
    
    file_pass_name = create_file_pass(pass_, passTypeIdentifier, serialNumber)

    return send_file(file_pass_name)


def delete_device_li_db(deviceLI):
    pass


def delete_library_pass_db(library_pass):
    pass


def unregisterate_device(version, deviceLibraryIdentifier, passTypeIdentifier, serialNumber):
    deviceLI = get_device_library_identifier_db(deviceLibraryIdentifier, status=ACTIVE)
    if not deviceLI:
        return 'Device library identifier not found.', 404
    
    pass_type_identifier = get_pass_type_identifiers_db(passTypeIdentifier, status=ACTIVE)
    if not pass_type_identifier:
        return 'Pass type identifier not found.', 404

    pass_templates = get_pass_template_db(pass_type_identifier, status=ACTIVE)
    if not pass_templates:
        return 'Pass templates not found.', 404
    
    pass_ = get_pass_db(serialNumber, pass_templates, status=[ACTIVE, WAIT_FINISH_REG])
    if not pass_:
        return 'Pass not found.', 404
    if pass_['authentication_token'] != request.headers.get('Authorization'):
        return '', 401

    relations = get_library_pass_db(deviceLI['id'], pass_['id'], status=ACTIVE)
    if not relations:
        return 'Relations not found.', 404

    if len(deviceLI['libraryPasses']) == 1:
        delete_device_li_db(deviceLI)
    
    delete_library_pass_db(relations)

    return 'Ok'


def log_errors(version):
    data = request.json
    if 'logs' in data:
        with open('apple.log', 'a') as f:
            f.write(str(data['logs']))
    return 'Ok'

from main import app

import json
from unittest import TestCase, main, TextTestRunner


jwt_header = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXNoYm94X2lkIjoiNjc4NDY1MyIsInBhcnRuZXJfaWQiOiI3MzQ4NjczNDgiLCJ1c2VyX2lkIjoiMzI0MzI1In0.kNyJm8gXyNBPxUCYh_sA4THF9-ok9YV7kja1I0YGKs8'


class Tests(TestCase):
    
    def setUp(self):
        self.cli = app.test_client()

    def test_pass_get(self):
        response = self.cli.get('/pass')
        assert response.status_code == 200

    def test_pass_post(self):
        data = {
            'serial_number_id': 1,
            'parent_serial_number_id': 1
        }
        response = self.cli.post('/pass', json=data)
        assert response.status_code == 200

        response = self.cli.post('/pass')
        assert response.status_code == 400

if __name__ == "__main__":
    log_file = 'test_log.txt'
    f = open(log_file, "w")
    runner = TextTestRunner(f)
    main(testRunner=runner)
    f.close()

